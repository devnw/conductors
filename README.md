# Atomizer Communication Conductors Suite

[![CI](https://github.com/devnw/conductors/workflows/CI/badge.svg)](https://github.com/devnw/conductors/actions)
[![Go Report Card](https://goreportcard.com/badge/github.com/devnw/conductors)](https://goreportcard.com/report/github.com/devnw/conductors)
[![codecov](https://codecov.io/gh/devnw/conductors/branch/master/graph/badge.svg)](https://codecov.io/gh/devnw/conductors)
[![GoDoc](https://godoc.org/github.com/devnw/conductors?status.svg)](https://pkg.go.dev/github.com/devnw/conductors)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)

